var config = {
  paths: {
    // disable jQuery migrate console output
    'jquery/jquery-migrate': 'js/jquery-migrate',
    //Adding owlCarousel
    'owlcarousel': "js/owl-carousel",
    'swissclinic/customcarousel': "js/custom-carousel",
    //Mapping messages
    'Magento_Theme/js/view/messages': 'js/messages'
  },
  shim: {
 		'owlcarousel': ['jquery']
	}
}