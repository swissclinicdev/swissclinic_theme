/**
 * @api
 */
define([
    'jquery',
    'uiComponent',
    'Magento_Customer/js/customer-data',
    'underscore',
    'jquery/jquery-storageapi'
], function ($, Component, customerData, _) {
    'use strict';

    return Component.extend({
        defaults: {
            cookieMessages: [],
            messages: [],
            selector: '.messages > .message',
            isHidden: false,
            listens: {
                isHidden: 'onHiddenChange'
            }
        },

        /**
         * Extends Component object by storage observable messages.s
         */
        initialize: function () {
            this._super().initObservable;

            this.cookieMessages = _.unique($.cookieStorage.get('mage-messages'), 'text');
            this.messages = customerData.get('messages').extend({
                disposableCustomerData: 'messages'
            });

            // Force to clean obsolete messages
            if (!_.isEmpty(this.messages().messages)) {
                customerData.set('messages', {});
            }

            $.cookieStorage.set('mage-messages', '');
        },

        initObservable: function() {
            this._super()
                .observe('isHidden');

            return this;
        },

        isVisible: function() {
            console.log('visible')
            return this.isHidden(!_.isEmpty(this.messages().messages));
        },

        /**
         * @param {Boolean} isHidden
         */
        onHiddenChange: function(isHidden) {
            var self = this;

            console.log('change')

            // Hide message block if needed
            if (isHidden) {
                setTimeout(function () {
                    $(self.selector).hide('blind', function() {
                        return self.isHidden(false);
                    }, 500);
                }, 5000);
            }
        },
    });
});