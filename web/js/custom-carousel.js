define(["jquery", "owlcarousel"], function($, owlCarousel) {
  "use strict";

  return function(config, element) {
    $(element).owlCarousel(config);
  };
});
