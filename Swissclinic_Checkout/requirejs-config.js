var config = {
    map: {
        '*': {
            ordersummary:'Swissclinic_Checkout/js/ordersummary'
        }
    },
     config: {
        mixins: {
            'Magento_Checkout/js/model/step-navigator': {
                'Swissclinic_Checkout/js/model/step-navigator': true
            }
        }
    }
};
